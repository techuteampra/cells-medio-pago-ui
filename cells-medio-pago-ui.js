{
  const {
    html,
  } = Polymer;
  /**
    `<cells-medio-pago-ui>` Description.

    Example:

    ```html
    <cells-medio-pago-ui></cells-medio-pago-ui>
    ```

    ## Styling
    The following custom properties and mixins are available for styling:

    ### Custom Properties
    | Custom Property     | Selector | CSS Property | Value       |
    | ------------------- | -------- | ------------ | ----------- |
    | --cells-fontDefault | :host    | font-family  |  sans-serif |
    ### @apply
    | Mixins    | Selector | Value |
    | --------- | -------- | ----- |
    | --cells-medio-pago-ui | :host    | {} |

    * @customElement
    * @polymer
    * @extends {Polymer.Element}
    * @demo demo/index.html
  */
  class CellsMedioPagoUi extends Polymer.mixinBehaviors([ CellsBehaviors.i18nBehavior, ], Polymer.Element) {
    static get is() {
      return 'cells-medio-pago-ui';
    }

    static get properties() {
      return {
        mes: {
          type: Array,
          value: function() {
            return [{
              'label': 'Enero',
              'value': '1'
            },
            {
              'label': 'Febrero',
              'value': '2'
            },
            {
              'label': 'Marzo',
              'value': '3'
            },
            {
              'label': 'Abril',
              'value': '4'
            },
            {
              'label': 'Mayo',
              'value': '5'
            },
            {
              'label': 'Junio',
              'value': '6'
            },
            {
              'label': 'Julio',
              'value': '7'
            },
            {
              'label': 'Agosto',
              'value': '8'
            },
            {
              'label': 'Setiembre',
              'value': '9'
            },
            {
              'label': 'Octubre',
              'value': '10'
            },
            {
              'label': 'Noviembre',
              'value': '11'
            },
            {
              'label': 'Diciembre',
              'value': '12'
            }];
          }
        }
      };
    }


    static get template() {
      return html `
      <style include="cells-medio-pago-ui-styles cells-medio-pago-ui-shared-styles"></style>
      <slot></slot>
      
      <br>

     <fieldset name="tituloPagar">
      <label for="email">Elige el medio de pago</label>
      </fieldset>


     <fieldset> 
      <div style="display: table; width: 90%;">
        <div style="display:table-row">             
              <div style="display: table-cell; width: 55%; text-align: center;">
              
              <img class='img-content-card' 
                src="https://comedor.s3-sa-east-1.amazonaws.com/tipo-pago/opcion-tc-128.png" style="width:100px;">
                <cells-radio-button id="rbTarjeta" checked>Tarjeta</cells-radio-button>              
              </div>

              <div style="display: table-cell; width: 55%; text-align: center;">             
              <img class='img-content-card' 
                src="https://comedor.s3-sa-east-1.amazonaws.com/tipo-pago/opcion-lukita-128.png" style="width:100px;">
              <cells-radio-button id="rbLukita" disabled>Lukita</cells-radio-button>
              </div>

        </div>
      </div>
      </fieldset>     
       
     <fieldset name="personalInfo">
				<label for="titular">Titular</label>
				<input type="text" id="titular" placeholder="Nombre y Apellido">
			</fieldset>
			<fieldset name="cardInfo">
				<label for="cardNum" required>Número de tarjeta</label>
        <input type="tel" id="cardNum" name="numTrajeta" autocomplete="off" placeholder="0000 0000 0000 0000" required minlength="15" maxlength="15">       
        
        <label for="cardExp" required>Fecha de vencimiento</label>
        <input type="tel" id="cardExp" name="numCaducidad" autocomplete="off" placeholder="MM/YYYY" maxlength="7">
       
				<label for="cardCVC" required>CVV2</label>				
        <input type="password" id="cardCCV" name="cvvTarjeta" autocomplete="off" placeholder="***" maxlength="3">
        <br>
        <br>
      </fieldset>
      <br>         
        
      </fieldset>
    
      <cells-st-button class="btn primary">
          <button>Pagar</button>
    </cells-st-button>

    </div>
      `;
    }
  }

  customElements.define(CellsMedioPagoUi.is, CellsMedioPagoUi);
}